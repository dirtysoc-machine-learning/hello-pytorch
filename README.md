# PYTORCH-GPU

This repo contains some sample pytorch models that you can use with a NVIDIA GPU for CUDA acceleration. 

Requirements:
1. Anaconda
2. CUDA Toolkit (11.0)

Steps to Setup:
1. Create new CONDA env with the `requirements.txt` file.
2. Activate the new env.
3. Test out running several files.

